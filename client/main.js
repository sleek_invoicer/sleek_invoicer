import Vue from 'vue'
import { Meteor } from 'meteor/meteor';
import '../imports/ui/plugins'

import App from '../imports/ui/App.vue'

import Hello from '../imports/ui/components/Hello.vue'
import CustomerOverview from '../imports/ui/components/CustomerOverview.vue'
import CustomerDetailPage from '../imports/ui/components/customers/CustomerDetailPage.vue'

import InvoiceTemplateVariablesPage from '../imports/ui/components/InvoiceTemplateVariablesPage.vue'
import UserOverview from '../imports/ui/components/UserOverview.vue'
import CompanyOverview from '../imports/ui/components/CompanyOverview.vue'
import CompanyDetailPage from '../imports/ui/components/companies/CompanyDetailPage.vue'
import UserProfilePage from '../imports/ui/components/UserProfilePage.vue'
import UserPublicProfilePage from '../imports/ui/components/UserPublicProfilePage.vue'
import InvoicesOverview from '../imports/ui/components/InvoicesOverview.vue'
import InvoicesDetailPage from '../imports/ui/components/invoices/InvoiceDetailPage.vue'

import InvoiceTemplatePage from "../imports/ui/components/InvoiceTemplatePage.vue"
import InvoiceTemplatesDetailPage from "../imports/ui/components/invoiceTemplates/InvoiceTemplatesDetailPage.vue"

import Howto from "../imports/ui/components/Howto.vue"

import { RouterFactory, nativeScrollBehavior } from 'meteor/akryum:vue-router2'

const routerFactory = new RouterFactory({
  mode: 'history',
  scrollBehavior: nativeScrollBehavior,
})

RouterFactory.configure(factory => {
  // Simple routes
  factory.addRoutes([
    {
      path: '/',
      name: 'howto',
      component: Howto,
    },
    {
      path: '/oldhome',
      name: 'home',
      component: Hello,
    },
    {
      path: '/variablesDocumentation',
      name: 'variablesDocumentation',
      component: InvoiceTemplateVariablesPage,
    },
    {
      path: '/invoices',
      name: 'invoices',
      component: InvoicesOverview,
    },
    {
      path: '/invoices/:id',
      name: 'invoiceDetail',
      component: InvoicesDetailPage,
    },
    {
      path: '/customers',
      name: 'customers',
      component: CustomerOverview,
    },
    {
      path: '/customers/:id',
      name: 'customerDetail',
      component: CustomerDetailPage,
    },
    {
      path: '/companies',
      name: 'companies',
      component: CompanyOverview,
    },
    {
      path: '/companies/:id',
      name: 'companyDetail',
      component: CompanyDetailPage,
    },
    {
      path: '/invoiceTemplates',
      name: 'invoiceTemplates',
      component: InvoiceTemplatePage,
    },
    {
      path: '/invoiceTemplates/:id',
      name: 'invoiceTemplatesDetail',
      component: InvoiceTemplatesDetailPage,
    },
    {
      path: '/management/users',
      name: 'users',
      component: UserOverview,
    },
    {
      path: '/management/self',
      name: 'personalPage',
      component: UserProfilePage,
    },
    {
      path: '/user/:id',
      name: 'personalPublicPage',
      component: UserPublicProfilePage,
    },
  ])
})

Meteor.startup(() => {
  const router = routerFactory.create()

  new Vue({
    router,
    ...App,
  }).$mount('#app');
});

