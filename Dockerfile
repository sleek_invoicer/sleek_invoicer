FROM node:14.19.3-buster


RUN apt update && apt-get install libfontconfig -y && rm -rf /var/lib/apt/lists/*



RUN mkdir /build
RUN ls -l

RUN ls -l ./
COPY buildArtifacts/sleek_invoicer.tar.gz /build 
RUN cd /build && tar -xf sleek_invoicer.tar.gz && rm sleek_invoicer.tar.gz

RUN cd /build/bundle/programs/server && npm install

COPY entrypoint.sh /build/bundle

ENTRYPOINT ["sh", "/build/bundle/entrypoint.sh"]
