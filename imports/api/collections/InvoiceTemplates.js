import { Mongo } from 'meteor/mongo';

export default new Mongo.Collection('invoiceTemplates');

// todo: actually validate the schema
const schema = {
    name: { type: String },
    companyId: { type: String },
    description: { type: String },
    template: { type: String },
    header: { type: Object },
    footer: { type: Object },
}