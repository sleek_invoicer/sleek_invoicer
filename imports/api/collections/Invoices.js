import { Mongo } from 'meteor/mongo';

export default new Mongo.Collection('invoices');


const schema = {
    customerId: { type: String },
    companyId: { type: String },
    date: { type: Date },
    number: {type: Number},
}