import { Mongo } from 'meteor/mongo';

export default new Mongo.Collection('countries');

// todo: Read only
const schema = {
    name: { type: String } ,
}