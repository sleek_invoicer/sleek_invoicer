import { Mongo } from 'meteor/mongo';

export default new Mongo.Collection('customers');

// todo: actually validate the schema
const schema = {
    name: { type: String },
    address: { type: Object, }, // address schema
    invoiceAddress: { type: Object, }, // address schema
    phone: { type: String },
    mobile: { type: String },
    email: { type: String },
    company: { type: String }, // the tennant-company the client belongs to 
}

const addressSchema = {
    street: { type: String },
    zipCode: { type: String },
    city: { type: String },
    country: { type: String },
}