import { Mongo } from 'meteor/mongo';

export default new Mongo.Collection('companies');

// todo: actually validate the schema
const schema = {
    name: { type: String },
    address: { type: Object, }, // address schema
    invoiceAddress: { type: Object, }, // address schema
    phone: { type: String },
    mobile: { type: String },
    email: { type: String },
    users: { type: Array }, // list of user ids that belong to this company
    public: { type: Boolean }, // if this company is in the public index or not 
}

const addressSchema = {
    street: { type: String },
    zipCode: { type: String },
    city: { type: String },
    country: { type: String },
}