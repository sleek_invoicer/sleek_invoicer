import Companies from '../collections/Companies';
import Customers from '../collections/Customers';
import { publishComposite } from 'meteor/reywood:publish-composite';

if (Meteor.isServer) {
    publishComposite('customers', function () {
        var loggedInUser = Meteor.user();

        return {
            find() {
                if (!this.userId) {
                    return null;
                }


                var allowedCompanyIds = Companies.find({
                    users: { $in: [loggedInUser._id] }
                }).fetch().map(company => {
                    return company._id;
                })

                if (allowedCompanyIds.includes(loggedInUser.activeCompany)) {
                    return Meteor.users.find({ _id: loggedInUser._id });
                } else {
                    return null;
                }

            },
            children: [{
                find(user) {
                    if (user.hasOwnProperty("activeCompany")) {
                        let customers = Customers.find(
                            { company: user.activeCompany },

                        )
                        return customers;
                    } else {
                        return;
                    }

                },
            }],
        }
    });


}

