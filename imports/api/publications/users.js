Meteor.publish('allUsers', function () {
    // Select only the users that match the array of IDs passed in
    const selector = {};

    const options = { activeCompany: 1 };

    return Meteor.users.find(selector, options);
});

// Deny all client-side updates to user documents
Meteor.users.deny({
    update() { return true; }
});