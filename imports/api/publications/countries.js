import { Meteor } from 'meteor/meteor';
import Countries from '../collections/Countries'


Meteor.publish('allCountries', function () {
    return Countries.find({}, {sort: {name: 1}});
  });
  