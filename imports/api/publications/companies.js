import Companies from '../collections/Companies';


Meteor.publish('companies', function () {
    if (!this.userId) {
        return this.ready();
    }

    var loggedInUser = Meteor.user();
    if (Roles.userIsInRole(loggedInUser, ['admin'], Roles.GLOBAL_GROUP)) {
        return Companies.find();
    }

    return Companies.find({
        $or: [
            { users: { $in: [loggedInUser._id] } },
            { public: true }
        ]
    })
});


