Meteor.publish(null, function () {
    var loggedInUser = Meteor.user();

    if (!Roles.userIsInRole(loggedInUser, ['admin'], Roles.GLOBAL_GROUP)) {
        this.ready();
    }
    // only admins can see roles
    return Meteor.roles.find({});
});

Meteor.publish(null, function () {

    var loggedInUser = Meteor.user();
    if (!Roles.userIsInRole(loggedInUser, ['admin'], Roles.GLOBAL_GROUP)) {
        this.ready();
    }
    // only admins can see the role assignments
    return Meteor.roleAssignment.find({});
})