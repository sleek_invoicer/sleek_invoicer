import { check } from 'meteor/check';

if (Meteor.isServer) {

    Meteor.methods({
        /**
         * Update a user's roles.
         *
         * @param {Object} targetUserId Id of user to update.
         * @param {Array} roles User's new roles.
         * @param {String} scope Company to update roles for.
         */
        'user.updateRoles'(targetUserId, roles, scope) {
            console.log(roles);
            check(targetUserId, String);
            check(roles, [String]);
            //check(scope, String);

            // only admins are allowed to upate roles of a user.
            var loggedInUser = Meteor.user();

            if (!loggedInUser ||
                !Roles.userIsInRole(loggedInUser,
                    ['admin'], scope)) {
                throw new Meteor.Error('access-denied', "Access denied");
            }

            Roles.setUserRoles(targetUserId, roles, scope);
        }
    })
}

