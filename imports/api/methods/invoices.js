import Invoices from '../collections/Invoices';
import Companies from '../collections/Companies';
import Customers from '../collections/Customers';
import {check} from 'meteor/check';
import InvoiceTemplates from "../collections/InvoiceTemplates";

if (Meteor.isServer) {
    Meteor.methods({
        'invoice.add'(data) {
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }

            var allowedCompanyIds = Companies.find({
                users: {$in: [loggedInUser._id]}
            }).fetch().map(company => {
                return company._id;
            })
            if (
                loggedInUser.hasOwnProperty("activeCompany") &&
                allowedCompanyIds.includes(loggedInUser.activeCompany)
            ) {
                let invoiceObject = {...data};
                invoiceObject.companyId = loggedInUser.activeCompany;

                var company = Companies.findOne({
                    _id: {$eq: invoiceObject.companyId},
                })
                invoiceObject.company = company;


                var customer = Customers.findOne({
                    _id: {$eq: invoiceObject.customerId},
                })

                invoiceObject.customer = customer;


                var previousInvoices = Invoices.find({
                    companyId: {$eq: invoiceObject.companyId},
                })

                var maxInvoiceNumber = 0;
                previousInvoices.forEach((invoice) => {
                    if (invoice.hasOwnProperty("number")) {
                        if (invoice.number > maxInvoiceNumber) {
                            maxInvoiceNumber = invoice.number;
                        }
                    }
                });
                invoiceObject.number = maxInvoiceNumber + 1;

                Invoices.insert(
                    invoiceObject,
                    function (error, invoiceId) {
                        if (invoiceId) {
                            Meteor.call("invoice.calculate", invoiceId);
                        }
                    }
                );

            }
        },
        'invoice.updateInvoiceDate'(invoiceId, invoiceDate) {
            console.log("invoice.update")
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (invoice) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({_id: invoice.companyId});
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Invoices.update(
                            {_id: invoiceId},
                            {$set: {date: invoiceDate}}
                        );
                        Meteor.call("invoice.calculate", invoiceId);
                    } else {
                        throw new Meteor.Error('access-denied', "Company mismatch");
                    }
                } else {
                    console.log("no company found for id" + invoice.companyId)
                }
            } else {
                console.log("no invoice found for id" + invoiceId);
            }

        },
        'invoice.addInvoiceElement'(invoiceId, newInvoiceElement) {
            console.log("invoice.addInvoiceElement");
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (invoice) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({_id: invoice.companyId});
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {


                        var invoiceElements = [];
                        if (invoice.hasOwnProperty("invoiceElements")) {
                            invoiceElements = invoice.invoiceElements;
                        }

                        var maxId = 0;
                        invoiceElements.forEach((element) => {
                            if (element.hasOwnProperty("id")) {
                                if (element.id > maxId) {
                                    maxId = element.id;
                                }
                            }
                        });
                        newInvoiceElement.id = maxId + 1;
                        invoiceElements.push(newInvoiceElement);

                        Invoices.update(
                            {_id: invoiceId},
                            {$set: {invoiceElements: invoiceElements}}
                        );
                        Meteor.call("invoice.calculate", invoiceId);
                    }
                }
            }
        },
        'invoice.deleteInvoiceElement'(invoiceId, invoiceElementId) {
            console.log("invoice.deleteInvoiceElement");
            check(invoiceId, String);
            check(invoiceElementId, Number);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (invoice) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({_id: invoice.companyId});
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        var invoiceElements = invoice.invoiceElements;
                        invoiceElements = invoiceElements.filter(function (element) {
                            return element.id !== invoiceElementId;
                        });
                        Invoices.update(
                            {_id: invoiceId},
                            {$set: {invoiceElements: invoiceElements}}
                        );
                        Meteor.call("invoice.calculate", invoiceId);
                    }
                }
            }
        },


        'invoice.finalize'(invoiceId) {

            console.log("invoice.finalize");
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (invoice) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({_id: invoice.companyId});
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {


                        Meteor.call(
                            "invoice.calculate",
                            invoiceId,
                            (error, result) => {
                                Meteor.call(
                                    "render.pdf_creator",
                                    invoice.invoiceTemplate.template,
                                    invoice.invoiceTemplate.options,
                                    invoice,
                                    (error, result) => {
                                        if (error) {
                                            console.log(error);
                                            alert(error);
                                        }
                                        if (result) {
                                            Invoices.update(
                                                {_id: invoiceId},
                                                {
                                                    $set: {
                                                        b64Pdf: result,
                                                        finalizedAt: new Date()
                                                    }
                                                }
                                            );
                                        }
                                    }
                                );
                            });


                    }
                }
            }


        },

        'invoice.calculate'(invoiceId) {
            // calculate amounts in invoice and its elements
            console.log("invoice.calculate");
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (invoice) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({_id: invoice.companyId});
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {


                        var localeString = 'en-GB';
                        if (invoice.hasOwnProperty("localeString")) {
                            console.log("using provided locale string")
                            localeString = invoice.localeString;

                        }
                        console.log(localeString);


                        const formatter = new Intl.NumberFormat(localeString, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                        });

                        var invoiceElements = invoice.invoiceElements;

                        var total = 0;
                        var totalDisplay = "0";

                        if (invoiceElements) {
                            invoiceElements.forEach((element) => {
                                // calculate priceWithTax

                                element.sumPrice = element.price * element.amount;

                                element.discountedPrice = element.sumPrice * (1 - element.discount / 100);

                                element.taxedPrice = element.discountedPrice * (1 + element.taxPercentage / 100);

                                element.priceWithTax =
                                    element.price *
                                    element.amount *
                                    (1 - element.discount / 100) *
                                    (1 + element.taxPercentage / 100);

                                // round to two decimals
                                element.priceWithTax = Math.round(element.priceWithTax * 100) / 100;
                                element.priceDisplay = formatter.format(element.priceWithTax)
                                total += element.priceWithTax;
                            });
                        }

                        total = Math.round(total * 100) / 100;
                        totalDisplay = formatter.format(total)


                        var dateString = null;
                        var year = null;
                        var month = null;
                        var periodStartString = null;
                        var periodEndString = null;
                        var finalizedAtString = null;

                        if (invoice.date) {
                            dateString = invoice.date.toLocaleDateString(localeString, {timeZone: 'UTC'})
                            year = invoice.date.getFullYear();
                            month = invoice.date.getMonth() + 1;
                        }
                        if (invoice.periodStart) {
                            periodStartString = invoice.periodStart.toLocaleDateString(localeString, {timeZone: 'UTC'})
                        }
                        if (invoice.periodEnd) {
                            periodEndString = invoice.periodEnd.toLocaleDateString(localeString, {timeZone: 'UTC'})
                        }
                        if (invoice.finalizedAt) {
                            finalizedAtString = invoice.date.toLocaleDateString(localeString, {timeZone: 'UTC'})
                        }

                        Invoices.update(
                            {_id: invoiceId},
                            {
                                $set: {
                                    year: year,
                                    month: month,
                                    dateString: dateString,
                                    finalizedAtString: finalizedAtString,
                                    periodStartString: periodStartString,
                                    periodEndString: periodEndString,
                                    total: total,
                                    totalDisplay: totalDisplay,
                                    invoiceElements: invoiceElements
                                }
                            }
                        );
                    }
                }
            }
        },
        'invoice.refreshTemplate'(invoiceId) {
            console.log("invoice.refreshTemplate")
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            var allowedCompanyIds = Companies.find({
                users: {$in: [loggedInUser._id]}
            }).fetch().map(company => {
                return company._id;
            })
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (allowedCompanyIds.includes(invoice.companyId)) {
                if (invoice.hasOwnProperty("invoiceTemplate")) {
                    if (invoice.invoiceTemplate.hasOwnProperty("_id")) {
                        var templateId = invoice.invoiceTemplate._id;
                        let template = InvoiceTemplates.findOne({_id: templateId});
                        if (template) {
                            Invoices.update(
                                {_id: invoiceId},
                                {
                                    $set: {
                                        invoiceTemplate: template,
                                    }
                                }
                            );
                        }
                    }
                }
            }
        },
        'invoice.refreshCustomer'(invoiceId) {
            console.log("invoice.refreshCustomer")
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            var allowedCompanyIds = Companies.find({
                users: {$in: [loggedInUser._id]}
            }).fetch().map(company => {
                return company._id;
            })
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be edited.")
            }
            if (allowedCompanyIds.includes(invoice.companyId)) {
                if (invoice.hasOwnProperty("customer")) {
                    if (invoice.customer.hasOwnProperty("_id")) {
                        var customerId = invoice.customer._id;
                        let customer = Customers.findOne({_id: customerId});
                        if (customer) {
                            Invoices.update(
                                {_id: invoiceId},
                                {
                                    $set: {
                                        customer: customer,
                                    }
                                }
                            );
                        }
                    }
                }
            }
        },
        'invoice.remove'(invoiceId) {
            check(invoiceId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            var allowedCompanyIds = Companies.find({
                users: {$in: [loggedInUser._id]}
            }).fetch().map(company => {
                return company._id;
            })
            let invoice = Invoices.findOne({_id: invoiceId});
            if (invoice.finalizedAt) {
                throw new Meteor.Error('acces-denied', "This invoice is finalized and cannot be deleted.")
            }
            if (allowedCompanyIds.includes(invoice.companyId)) {
                Invoices.remove({
                    _id: invoiceId
                });
            }
        },
    });
}