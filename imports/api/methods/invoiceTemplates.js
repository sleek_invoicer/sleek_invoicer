import InvoiceTemplates from '../collections/InvoiceTemplates';
import Companies from '../collections/Companies';
import { check } from 'meteor/check';

if (Meteor.isServer) {
    Meteor.methods({
        'invoiceTemplate.add'(data) {
            console.log("invoiceTemplate.add");
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }

            var allowedCompanyIds = Companies.find({
                users: { $in: [loggedInUser._id] }
            }).fetch().map(company => {
                return company._id;
            })

            if (
                loggedInUser.hasOwnProperty("activeCompany") &&
                allowedCompanyIds.includes(loggedInUser.activeCompany)
            ) {
                let invoiceObject = { ...data };
                invoiceObject.companyId = loggedInUser.activeCompany;
                InvoiceTemplates.insert(
                    invoiceObject,
                );
            }
        },
        'invoiceTemplate.update'(invoiceTemplateId, data) {
            check(invoiceTemplateId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoiceTemplate = InvoiceTemplates.findOne({ _id: invoiceTemplateId });
            if (invoiceTemplate) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: invoiceTemplate.companyId });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        InvoiceTemplates.update(
                            { _id: invoiceTemplateId },
                            { $set: data }
                        );
                    }
                }
            }

        },
        'invoiceTemplate.updateTemplate'(invoiceTemplateId, template) {
            check(invoiceTemplateId, String);
            check(template, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoiceTemplate = InvoiceTemplates.findOne({ _id: invoiceTemplateId });
            if (invoiceTemplate) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: invoiceTemplate.companyId });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        InvoiceTemplates.update(
                            { _id: invoiceTemplateId },
                            { $set: { template: template } }
                        );
                    }
                }
            }

        },
        'invoiceTemplate.updateHeader'(invoiceTemplateId, header) {
            check(invoiceTemplateId, String);

            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoiceTemplate = InvoiceTemplates.findOne({ _id: invoiceTemplateId });
            if (invoiceTemplate) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: invoiceTemplate.companyId });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        var options = invoiceTemplate.options;
                        options.header = header;
                        InvoiceTemplates.update(
                            { _id: invoiceTemplateId },
                            { $set: { options: options } }
                        );
                    }
                }
            }

        },
        'invoiceTemplate.updateFooter'(invoiceTemplateId, footer) {
            check(invoiceTemplateId, String);

            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let invoiceTemplate = InvoiceTemplates.findOne({ _id: invoiceTemplateId });
            if (invoiceTemplate) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: invoiceTemplate.companyId });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        var options = invoiceTemplate.options;
                        options.footer = footer;
                        InvoiceTemplates.update(
                            { _id: invoiceTemplateId },
                            { $set: { options: options } }
                        );
                    }
                }
            }

        },
        'invoiceTemplate.remove'(invoiceTemplateId) {
            check(invoiceTemplateId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            var allowedCompanyIds = Companies.find({
                users: { $in: [loggedInUser._id] }
            }).fetch().map(company => {
                return company._id;
            })
            console.log(allowedCompanyIds);
            let invoiceTemplate = InvoiceTemplates.findOne({ _id: invoiceTemplateId });
            // customer belongs to a company of the user

            if (allowedCompanyIds.includes(invoiceTemplate.companyId)) {
                InvoiceTemplates.remove({
                    _id: invoiceTemplateId
                });
            }
        },
    });
}