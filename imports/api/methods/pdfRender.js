if (Meteor.isServer) {
    Meteor.methods({
        async 'render.pdf_creator'(html, options, data) {

            if (!html) {
                console.log("html is missing");
            }

            if (!options) {
                console.log("options is missing");
            }

            if (!data) {
                console.log("data is missing");
            }

            //https://www.npmjs.com/package/pdf-creator-node
            var pdf = require("pdf-creator-node");
            var doc = {
                html: html,
                data: data,
                type: "buffer",
            };
            const pdfBuffer = await pdf.create(doc, options)
            if (pdfBuffer) {
                var u8 = pdfBuffer; //uint8
                var b64 = Buffer.from(u8).toString('base64');
                return b64;
            }

        },
    })
}
