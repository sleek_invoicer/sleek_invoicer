import Customers from '../collections/Customers';
import Companies from '../collections/Companies';
import { check } from 'meteor/check';

if (Meteor.isServer) {
    Meteor.methods({
        'customer.add'(data) {
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }

            var allowedCompanyIds = Companies.find({
                users: { $in: [loggedInUser._id] }
            }).fetch().map(company => {
                return company._id;
            })
            if (
                loggedInUser.hasOwnProperty("activeCompany") &&
                allowedCompanyIds.includes(loggedInUser.activeCompany)
            ) {
                let customerObject = { ...data };
                customerObject.contactPersons = [];
                customerObject.company = loggedInUser.activeCompany;
                Customers.insert(
                    customerObject,
                );
            }
        },
        'customer.update'(customerId, data) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let customer = Customers.findOne({ _id: customerId });
            if (customer) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: customer.company });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Customers.update(
                            { _id: customerId },
                            { $set: data }
                        );
                    }
                }
            }

        },
        'customer.addContractPerson'(customerId, contractPersons) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let customer = Customers.findOne({ _id: customerId });
            if (customer) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: customer.company });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Customers.update(
                            { _id: customerId },
                            { $set: { contractPersons: contractPersons } }
                        );
                    }
                }
            }
        },
        'customer.setContactPersons'(customerId, contactPersons) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let customer = Customers.findOne({ _id: customerId });
            if (customer) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: customer.company });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Customers.update(
                            { _id: customerId },
                            { $set: { contactPersons: contactPersons } }
                        );
                    }
                }
            }
        },
        'customer.updateAddress'(customerId, address) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let customer = Customers.findOne({ _id: customerId });
            if (customer) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: customer.company });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Customers.update(
                            { _id: customerId },
                            { $set: { address: address } }
                        );
                    }
                }
            }

        },
        'customer.updateInvoiceAddress'(customerId, address) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let customer = Customers.findOne({ _id: customerId });
            if (customer) {
                // only users belonging to the company or admins may delete it
                let company = Companies.findOne({ _id: customer.company });
                if (company) {
                    if (
                        Roles.userIsInRole(loggedInUser,
                            ['admin'], Roles.GLOBAL_GROUP) ||
                        company.users.indexOf(loggedInUser._id) >= 0
                    ) {
                        Customers.update(
                            { _id: customerId },
                            { $set: { invoiceAddress: address } }
                        );
                    }
                }
            }

        },
        'customer.remove'(customerId) {
            check(customerId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            var allowedCompanyIds = Companies.find({
                users: { $in: [loggedInUser._id] }
            }).fetch().map(company => {
                return company._id;
            })
            console.log(allowedCompanyIds);
            let customer = Customers.findOne({ _id: customerId });
            console.log(customer.company)
            // customer belongs to a company of the user
            console.log(allowedCompanyIds.includes(customer.company))
            console.log(allowedCompanyIds.indexOf(customer.company))
            if (allowedCompanyIds.includes(customer.company)) {
                Customers.remove({
                    _id: customerId
                });
            }
        },
    });
}