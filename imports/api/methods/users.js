import { check } from "meteor/check";

if (Meteor.isServer) {
  Meteor.methods({
    "user.create"(username, password) {
      if (!this.userId) {
        throw new Meteor.Error("not-authorized");
      }

      // check that currently logged in user is admin
      var loggedInUser = Meteor.user();
      if (
        !loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      Accounts.createUser({ username: username, password: password });
    },
    "user.setUsername"(userId, username) {
      if (!this.userId) {
        throw new Meteor.Error("not-authorized");
      }

      // check that currently logged in user is admin
      var loggedInUser = Meteor.user();
      if (
        !loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      Accounts.setUsername(userId, username);
    },
    "user.setPassword"(userId, password) {
      check(userId, String);
      check(password, String);
      // check that currently logged in user is admin
      var loggedInUser = Meteor.user();
      if (
        loggedInUser ||
        Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        Accounts.setPassword(userId, password);
      } else {
        throw new Meteor.Error("access-denied", "Access denied");
      }
    },
    "user.addEmail"(userId, email) {
      check(userId, String);
      check(email, String);
      var loggedInUser = Meteor.user();
      if (
        !loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        throw new Meteor.Error("access-denied", "Access denied");
      }
      Accounts.addEmail(userId, email);
    },
    "user.removeEmail"(userId, email) {
      check(userId, String);
      check(email, String);
      var loggedInUser = Meteor.user();
      if (
        !loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        throw new Meteor.Error("access-denied", "Access denied");
      }
      Accounts.removeEmail(userId, email);
    },
    "user.setActiveCompany"(userId, companyId) {
      console.log("user.setActiveCompany");
      check(userId, String);
      var loggedInUser = Meteor.user();
      if (!loggedInUser || loggedInUser._id !== userId) {
        throw new Meteor.Error("access-denied", "Access denied");
      }
      console.log(companyId);
      Meteor.users.update(userId, {
        $set: {
          activeCompany: companyId,
        },
      });
    },
    "user.removeActiveCompanyAfterLeavingCompany"(userId, companyId) {
      check(userId, String);
      check(companyId, String);
      var loggedInUser = Meteor.user();
      if (!loggedInUser) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      Meteor.users.update(
        { _id: userId, activeCompany: companyId },
        {
          $set: {
            activeCompany: null,
          },
        }
      );
    },
    "user.remove"(userId) {
      check(userId, String);

      // check that currently logged in user is admin
      var loggedInUser = Meteor.user();
      if (
        !loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      if (!this.userId) {
        throw new Meteor.Error("not-authorized");
      }
      Meteor.users.remove(userId);
    },

    "user.setFirstName"(userID, newFirstName) {
      check(userID, String);
      check(newFirstName, String);

      var loggedInUser = Meteor.user();
      if (!loggedInUser) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      if (loggedInUser._id == userID || Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)) {
        Meteor.users.update(
          { _id: userID },
          {
            $set: {
              firstName: newFirstName,
            },
          });
      }
    },

    "user.setLastName"(userID, newLastName) {
      check(userID, String);
      check(newLastName, String);


      var loggedInUser = Meteor.user();
      if (!loggedInUser) {
        throw new Meteor.Error("access-denied", "Access denied");
      } else {
        if (
          loggedInUser._id == userID ||
          Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
        ) {
          Meteor.users.update(
            { _id: userID },
            {
              $set: {
                lastName: newLastName,
              },
            });
        }
      }
    },

    'user.updateAddress'(userId, address) {
      check(userId, String);
      var loggedInUser = Meteor.user();
      if (!loggedInUser) {
        throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
      }
      let user = Meteor.users.findOne({ _id: userId });
      if (user || Roles.userIsInRole(loggedInUser,
        ['admin'], Roles.GLOBAL_GROUP)) {
        Meteor.users.update(
          { _id: userId },
          { $set: { address: address } }
        );
      }
    },

    "user.setPublicMarkdown"(userID, markdown) {
      check(userID, String);
      check(markdown, String);
      var loggedInUser = Meteor.user();
      if (!loggedInUser) {
        throw new Meteor.Error("access-denied", "Access denied");
      }

      if (
        loggedInUser._id == userID ||
        Roles.userIsInRole(loggedInUser, ["admin"], Roles.GLOBAL_GROUP)
      ) {
        Meteor.users.update(
          { _id: userID },
          {
            $set: {
              publicMarkdown: markdown,
            },
          });
      }


    },
  })
}
