import Companies from '../collections/Companies';
import { check } from 'meteor/check';
import Invoices from '../collections/Invoices';
import Customers from '../collections/Customers';
import InvoiceTemplates from '../collections/InvoiceTemplates';


if (Meteor.isServer) {
    Meteor.methods({
        'company.add'(data) {
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            let companyObject = { ...data };
            companyObject.users = [loggedInUser._id];
            companyObject.public = false;

            Companies.insert(
                companyObject,
                function (error, companyId) {
                    console.log(companyId)
                    if (companyId) {
                        // if the active company is not set, set it after joining one.
                        Meteor.call("user.setActiveCompany", loggedInUser._id, companyId);
                    }
                }
            );

        },
        'company.togglePublic'(companyId) {
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) || company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    Companies.update(
                        { _id: companyId },
                        { $set: { public: !company.public } }
                    );
                }
            }
        },
        'company.update'(companyId, data) {
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    Companies.update(
                        { _id: companyId },
                        { $set: data }
                    );
                }
            }
        },
        'company.updateAddress'(companyId, address) {
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    Companies.update(
                        { _id: companyId },
                        { $set: { address: address } }
                    );
                }
            }
        },
        'company.updateInvoiceAddress'(companyId, address) {
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    Companies.update(
                        { _id: companyId },
                        { $set: { invoiceAddress: address } }
                    );
                }
            }
        },
        'company.addUser'(companyId, userId) {
            check(companyId, String);
            check(userId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0 ||
                    company.public
                ) {
                    let updatedUserList = [...company.users]
                    updatedUserList.push(userId);
                    Companies.update(
                        { _id: companyId },
                        { $set: { users: updatedUserList } }
                    );
                    if (!loggedInUser.activeCompany) {
                        // if the active company is not set, set it after joining one.
                        Meteor.call("user.setActiveCompany", loggedInUser._id, company._id);
                    }
                }
            }
        },

        'company.removeUser'(companyId, userId) {
            check(companyId, String);
            check(userId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) || company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    let updatedUserList = company.users.filter(e => {
                        return e !== userId;
                    })
                    Companies.update(
                        { _id: companyId },
                        { $set: { users: updatedUserList } }
                    );
                    Meteor.call("user.removeActiveCompanyAfterLeavingCompany", userId, companyId)
                }
            }
        },
        'company.addCurrency'(companyId, newCurrency) {
            console.log("company.addCurrency");
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let company = Companies.findOne({ _id: companyId });

            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    currencies = [];
                    if (company.hasOwnProperty("currencies")) {
                        currencies = company.currencies;
                    }

                    var maxId = 0;
                    currencies.forEach((element) => {
                        if (element.hasOwnProperty("id")) {
                            if (element.id > maxId) {
                                maxId = element.id;
                            }
                        }
                    });
                    newCurrency.id = maxId + 1;
                    currencies.push(newCurrency);

                    Companies.update(
                        { _id: companyId },
                        { $set: { currencies: currencies } }
                    );
                }
            }
        },
        'company.removeCurrency'(companyId, currencyId) {
            console.log("company.removeCurrency");
            check(companyId, String);
            check(currencyId, Number);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "This action is only allowed for a logged in user");
            }
            let company = Companies.findOne({ _id: companyId });

            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) ||
                    company.users.indexOf(loggedInUser._id) >= 0
                ) {


                    currencies = [];
                    if (company.hasOwnProperty("currencies")) {
                        currencies = company.currencies;
                    }
                    currencies = currencies.filter(function (element) {
                        return element.id !== currencyId;
                    });
                    Companies.update(
                        { _id: companyId },
                        { $set: { currencies: currencies } }
                    );
                }
            }
        },

        'company.remove'(companyId) {
            // if a company is deleted, also delete all associated entities with it
            check(companyId, String);
            var loggedInUser = Meteor.user();
            if (!loggedInUser) {
                throw new Meteor.Error('access-denied', "Companies can only be created by logged in users.");
            }
            // only users belonging to the company or admins may delete it
            let company = Companies.findOne({ _id: companyId });
            if (company) {
                if (
                    Roles.userIsInRole(loggedInUser,
                        ['admin'], Roles.GLOBAL_GROUP) || company.users.indexOf(loggedInUser._id) >= 0
                ) {
                    //todo: delete associated entities!!! (needs to be extended as the application grows!)
                    InvoiceTemplates.remove({ companyId: companyId });
                    Invoices.remove({ companyId: companyId });
                    Customers.remove({ company: companyId });

                    Companies.remove({ _id: companyId });
                }
            }


        }
    });

}