import { Meteor } from 'meteor/meteor';
import Countries from './collections/Countries.js'; 
import { data } from 'autoprefixer';


Meteor.startup(() => {
    Countries.remove({});
    if (Countries.find().count() === 0){
      var countriesDB = require('countries-db');
      const all_countries = countriesDB.getAllCountries('name');
      
      for (country_id in all_countries){
        country = countriesDB.getCountry(country_id)

        Countries.insert(
          { 'name': country.name }
        );
      
      };

    };

})