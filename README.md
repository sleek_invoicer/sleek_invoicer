# Sleek Invoicer

> Meteor, Vuejs, MustacheJS, html2pdf/jsPDF

A multi-tennant tool to create and manage invoices to various clients. Invoices are generated as downloadable pdf. Configurable reminders of unpaid invoices make it easy to keep track of outstanding payments. Dashboards reveal a detailed financial overview.


## Development

### Start the application

1. Clone the repository
1. `meteor npm install`
1. `meteor`

The application is running at http://localhost:3000
### Access the dev database

The easiest way to access the development mongodb is to call `meteor mongo`, which yields a db shell and accepts the usual mongodb commands. Currently, this is the only way to delete data from the database.

### Build & Deploy
First, the meteor project is built into a tar.gz that has a certain version of nodejs as only dependency.

```
meteor build ./buildArtifacts
```
Then, the resulting build artifact can be run using plain nodejs or packaged into a docker container image.

> Todo...