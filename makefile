run:
	meteor

install:
	meteor npm install

build:
	rm -rf ./buildArtifacts
	meteor build ./buildArtifacts

docker_images:
	docker build -t registry.gitlab.com/sleek_invoicer/sleek_invoicer .

push_docker_images:
	docker push registry.gitlab.com/sleek_invoicer/sleek_invoicer

pipeline:
	make build
	make docker_images
	make push_docker_images