import '../imports/api/fixtures'
import '../imports/api/methods'
import '../imports/api/publications'
import { Mongo } from 'meteor/mongo'


if (Meteor.isServer) {
    Meteor.startup(() => {
        // code to run on server at startup

        let admin_role = "admin";

        Roles.createRole(admin_role, { unlessExists: true });

        //Roles.createRole('testRole', { unlessExists: true });
        // add more roles here

        if (Meteor.users.find().count() === 0) {
            let admin_id = Accounts.createUser({
                username: 'admin',
                email: 'admin@sleekinvoicer.io',
                password: 'admin'
            });
            Roles.addUsersToRoles(admin_id, admin_role);
        }
    });
}